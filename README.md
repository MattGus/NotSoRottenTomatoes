# NotSoRottenTomatoes

Le but du projet est de réaliser une application – avec interface graphique pour la construction de graphes orientés et de diagrammes sagittaux 
  d'états finis, avec des facilités automatiques et semi-automatiques
  pour produire des diagrammes naturels, plaisants, et lisibles, exportés au
  ormat Latex/TikZ.

  Cette  application  a  pour  vocation  de  venir  en  aide  à  la  minorité  peu
  considérée  des  professeurs  de  théorie  des  langages,  qui  ont  très  envie
  d’illustrer leurs diapositives et polycopiés avec de nombreux et beaux
  diagrammes, mais qui n’ont pas le luxe de passer à chaque fois une heure
  à se reconnecter avec leur artiste intérieur afin d’imaginer comment
  l’automate devrait être disposé pour être “joli”, et encore une heure à se
  battre avec Latex/tikZ

Réalisé par :
* Matthieu Delorme
* Yohan Chirle
* Cristopher Roque
* Maxence Poisson
* Etienne Duhammel
* Romain Vergnaud 

avec la providentielle supervision de Vincent Hugot